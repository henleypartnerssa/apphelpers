# Changelog

All Notable changes to `AppHelpers` will be documented in this file.

## [0.0.1] 2016-05-19
### Added
- Project Init

## [0.0.3] 2016-05-19
### Added
- Updated "stepperLinks" function by removing appID parameter from function and output array.

## [0.0.4] 2016-05-20
### Added
- added env config to "genCaseNo" function.

### Deprecated
- Nothing

## [0.0.2] 2016-05-19
### Fixed
- Quick fixes to composer file.

### Removed
- Nothing

### Security
- Nothing
