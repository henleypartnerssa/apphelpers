<?php

namespace hpsadev\AppHelpers;

use \Illuminate\Support\Facades\Storage;
use App\Application;
use App\ProgramType;
use App\ApplicationType;
use App\Country;
use DB;
use App\Step;
use Carbon\Carbon;
use App\Util;
use App\Profile;
use Auth;

/**
 * Global Class static Helper functions
 *
 * @author      Gershon Koks <gershon.koks@henleyglobal.com>
 * @copyright   Henley & Partners Ltd
 * @license     For Henley & Partners use only.
 * @version     0.0.5
 */
class AppHelpers {

    /**
     * Used to lookup country code from file on disk.
     *
     * @deprecated no longer in use.
     * @since 0.0.1 Function Init
     * @param  string $country Country Name
     * @return string         Country Code
     */
    public static function lkpCoutrnyCode($country) {
        $code = null;
        $fileData = explode("\n", Storage::get('country_codes.dat'));
        foreach($fileData as $record) {
            $split = explode("|", $record);
            if (!isset($split[1])) {
                continue;
            }
            if ($country === $split[1]) {
                $code = $split[0];
                break;
            }
        }
        return $code;
    }

    /**
     * Generates Case Number
     *
     * @since   0.0.3 Added env config to function
     * @since   0.0.1 Function Init
     * @return  string new case number
     */
    public static function genCaseNo() {
        $userId     = Auth::id();
        $program    = AppHelpers::normalizeProgramName(env('PROGRAM_NAME'));
        $caseNO     = env('COMPANY_CODE') . '-' . $program . '-' . $userId;
        return $caseNO;
    }

    /**
     * Builds array of processing steps for View's and Controllers
     *
     * @since 0.0.1 Function Init
     * @param integer $currentStep Current Step .i.e view page
     * @return array                array of steps
     */
    public static function buildStepper($currentStep = 1, $app = null) {

        $steps = Step::orderBy('step_no', 'asc')->get()->toArray();
        $array = [];

        foreach($steps as $step) {

            $state = "disabled";

            if ($app && $app->step === 16) {
                $state = "completed";
            }else if ($step['step_no'] < $currentStep) {
                $state = "completed";
            } else if ($step['step_no'] === $currentStep) {
                $state = "active";
            }

            array_push($array, [
                'title'         => $step['title'],
                'description'   => $step['description'],
                'state'         => $state,
                'step_no'       => $step['step_no']
            ]);
        }

        return $array;

    }

    /**
     * Used for first time deployments to pupulate Coutries table
     * with latest country names and codes by selecting data from REST API
     *
     * @since 0.0.1 Function Init
     * @return integer indicator
     */
    public static function populateCountriesTable() {

        $counties = json_decode(file_get_contents('https://restcountries.eu/rest/v1/all'), true);

        $rows = [];

        foreach($counties as $country) {

            $record = ['name' => $country['name'], 'code' => strtolower($country['alpha2Code']), 'calling_code' => $country['callingCodes'][0], 'updated_at' => Carbon::now(), 'created_at' => Carbon::now()];
            array_push($rows, $record);
        }

        if (DB::table('countries')->insert($rows)) {
            return 1;
        } else {
            return 0;
        }
    }


    /**
     * Cast JSON Object to DB Array
     *
     * @since 0.0.1 Function Init
     * @param  string $data JSON Onbject
     * @return array       [JSON object data cast to array]
     */
    public static function castToDbArray($data) {
        $counter    = count($data);
        $start      = '{'.'"';
        $end        = '}';
        $stringHolder = "";
        $index = 0;
        for($i = 0; $i < $counter; $i++) {
            $index = $index + 1;
            if ($data[$i] !== "") {
                if ($index === $counter) {
                    $stringHolder = $stringHolder.$data[$i].'"';
                    break;
                } else {
                    $stringHolder = $stringHolder.$data[$i].'"'.','.'"';
                }
            }
        }
        return ($stringHolder === "") ? null : str_replace(',"}', '}', $start.$stringHolder.$end);
    }

    /**
     * Narmalise indavidual date fields by inserting
     * 0 charater in ivent of missing 0.
     *
     * @since 0.0.1 Function Init
     * @param  integer $year  date year
     * @param  integer $month date month
     * @param  integer $day   date day
     * @return [integer]      normalised date now containing missing 0
     */
    public static function normalizeDate($year,$month,$day) {
        $y = (strlen($year) === 1) ? '0'.$year : $year;
        $m = (strlen($month) === 1) ? '0'.$month : $month;
        $d = (strlen($day) === 1) ? '0'.$day : $day;
        return ($y.$m.$d);
    }

    /**
     * Lookup Code
     *
     * @since 0.0.1 Function Init
     * @param  string $program Country Name
     * @return string          Program Code
     */
    public static function normalizeProgramName($program) {
        $p['Antigua and Barbuda'] = 'AB';
        $p['Dominica'] = 'DM';
        return $p[$program];
    }

    /**
     * Appends currency name to total
     *
     * @since 0.0.1 Function Init
     * @param  string $amount      amount of money
     * @param  string $currentType currency name
     * @return string              appended value
     */
    public static function normalizeMoneyTotal($amount, $currentType) {
        $value = null;
        if ($currentType === 'dollar') {
            $value = "dollar-".$amount;
        } else if ($currentType === 'euro') {
            $value = "euro-".$amount;
        } else if ($currentType === 'pound') {
            $value = "pound-".$amount;
        }
        return $value;
    }

    /**
     * Get the currency name from total
     *
     * @since 0.0.1 Function Init
     * @param  string $total appended total money
     * @return string        currency name
     */
    public static function getCurrencyFromTotal($total) {
        $split = explode("-", $total);
        if ($split[0] === "dollar") {
            return 'dollar';
        } else if ($split[0] === "euro") {
            return 'euro';
        } else if ($split[0] === "pound") {
            return 'pound';
        }
    }

    /**
     * Strip the currency name from total amount of money
     *
     * @since 0.0.1 Function Init
     * @param  string $total appended total
     * @return mixed        total money.
     */
    public static function stripCurrency($total) {
        $split = explode("-", $total);
        return (!empty($split[1])) ? $split[1] : null;
    }

    /**
     * Strips leading 0 from total if number is of
     * 3 digits or less.
     *
     * @since 0.0.1 Function Init
     * @param  string $total total money
     * @return string        normalised total
     */
    public static function normQuoteTotal($total) {
        $split = explode('.', $total);
        $return;
        if (count($split[0]) === 1 && $split[0] === '0') {
            $return = $split[1];
        } else {
            $return = $total;
        }
        return $return;
    }

    /**
     * Get All URL's for pressing steps.
     *
     * @since 0.0.1 Function Init
     * @return array an array of processing URL's
     */
    public static function appProcessUrl() {
        $profile = Auth::user()->profiles()->where('belongs_to', 'Principal Applicant')->first();
        $data = [
            'url' => AppHelpers::stepperLinks($profile->profile_id, $profile->application_id)[$profile->application->step]
        ];
        return $data;
    }

    /**
     * Build appropriate URL as per paramenters specified.
     *
     * @since   0.0.3 Removed AppID Parameter from function and URI's
     * @since   0.0.1 Function Init
     * @param   integer $profileId  Profile ID
     * @param   integer $appId      Application ID
     * @return  string              Processing Step URL
     */
    public static function stepperLinks($profileId) {
        $link['1'] = '/users/application/select-program';
        $link['2'] = '/users/profile/'.$profileId.'/application/important-information';
        $link['3'] = '/users/application/dd-check';
        $link['4'] = '/users/application/payment-for-services';
        $link['5'] = '/users/profile/'.$profileId.'/application/personal-data';
        $link['6'] = '/users/profile/'.$profileId.'/application/upload-documents';
        $link['7'] = '/users/application/data-review';
        $link['8'] = '/users/profile/'.$profileId.'/application/gov-documents';
        $link['9'] = '/users/profile/'.$profileId.'/application/upload-notary';
        $link['10'] = '/users/application/balance-of-fees';
        $link['11'] = '/users/application/courier';
        $link['12'] = '/users/application/package-check';
        $link['13'] = '/users/application/submission';
        $link['14'] = '/users/application/investment-status';
        $link['15'] = '/users/application/oath';
        $link['16'] = '/users/application/collect-passport';
        return $link;
    }

    /**
     * Validates if all Applicatio Profiles have moved
     * beyond state 4.
     *
     * @since 0.0.1 Function Init
     * @param  User $user the authenticated User.
     * @return boolean       indicator
     */
    public static function valDataPages($user) {
        $totProfiles = $user->profiles()->count();
        $totComplete = 0;
        foreach ($user->profiles as $p) {
            if ($p->state === 5) {
                ++$totComplete;
            }
        }
        if ($totComplete === $totProfiles) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Sets the Application Processing Step
     *
     * @since   0.0.5 Function Init
     * @param   integer $stepNo  Specified step number.
     * @return  mixed            data saved successfully or not.
     */
    public static function updateAppStep($stepNo) {
        $app = (Auth::user()->profiles()->first()) ? Auth::user()->profiles()->first()->application : NULL;
        if($app) {
            if ($app->step === $stepNo || $app->step > $stepNo) {
                return;
            } else {
                $app->step = $stepNo;
                return $app->save();
            }
        } else {
            return;
        }
    }

    /**
     * Generates random string.
     * @since   0.0.6 Function Init
     * @param  integer $length length of charaters to return.
     * @return stirng          random string.
     */
    public static function randomStr($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
