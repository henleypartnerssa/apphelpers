<?php

namespace hpsadev\AppHelpers;

use Illuminate\Support\ServiceProvider;

class AppHelpersServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // nothing to do for this Package...
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        // nothing to do for this Package...
    }
}
